class FizzBuzz
	def go
		fizz_counter = 0
		buzz_counter = 0

		(1..100).each do |value|
			fizz_counter += 1
			buzz_counter += 1
			output = ""

			if fizz_counter == 3
				output += "Fizz"		
				fizz_counter = 0	
			end
			if buzz_counter == 5
				output += "Buzz"
				buzz_counter = 0
			end
			if output.empty? 
				output = value.to_s
			end

			puts output
		end
	end

end