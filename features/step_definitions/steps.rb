require_relative '../../fizzbuzz'

Given("we have ran the program") do
  @fizz_buzz = FizzBuzz.new
end

When("it starts") do
	@lines = capture_stdout_lines do
		@fizz_buzz.go
	end
end

Then("we see numbers") do |value|
	expect(extract_numbers(@lines)).to include(value) 
end

Then("on line {int} we see {string}") do |int, string|
	expect(@lines[int-1]).to eq(string)
end
