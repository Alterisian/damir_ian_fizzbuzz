Feature: Generate FizzBuzz numbers
	Scenario: Running program
		Given we have ran the program
		When it starts
		Then we see numbers
			"""
			1
			2
			4
			7
			8
			"""
		Then on line 3 we see 'Fizz'
		Then on line 5 we see 'Buzz'
		Then on line 6 we see 'Fizz'
		Then on line 9 we see 'Fizz'
		Then on line 10 we see 'Buzz'
		Then on line 15 we see 'FizzBuzz'
		Then on line 90 we see 'FizzBuzz'