def capture_stdout_lines
	io = StringIO.new("")
	previous_stdout = $stdout
	$stdout = io

	yield

	io.string.split
ensure
	$stdout = previous_stdout
	[]
end

def extract_numbers(lines)
	lines.grep(/\d+/).join("\n")
end